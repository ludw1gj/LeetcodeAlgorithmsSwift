//
//  listNode.swift
//  Leetcode Algorithms Swift
//

class ListNode {
    var val: Int
    var next: ListNode?
    
    init(_ val: Int) {
        self.val = val
        self.next = nil
    }
}

