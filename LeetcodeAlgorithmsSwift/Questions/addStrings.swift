/*
 415. Add Strings
 https://leetcode.com/problems/add-strings/description/
 
 Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.
 
 Note:
 1. The length of both num1 and num2 is < 5100.
 2. Both num1 and num2 contains only digits 0-9.
 3. Both num1 and num2 does not contain any leading zero.
 4. You must not use any built-in BigInteger library or convert the inputs to integer directly.
 
 Runtime: 126 ms
 */

func addStrings(_ num1: String, _ num2: String) -> String {
    let num1Array = num1.reversed().map { Int(String($0))! }
    let num2Array = num2.reversed().map { Int(String($0))! }
    var result = String()
    
    var i = 0, j = 0, sum = 0, carry = 0
    while i < num1Array.count || j < num2Array.count || carry != 0 {
        sum = carry
        
        if i < num1Array.count {
            sum += num1Array[i]
            i += 1
        }
        if j < num2Array.count {
            sum += num2Array[j]
            j += 1
        }
        
        carry = sum / 10
        sum %= 10
        result.append(String(sum))
    }
    
    return String(result.reversed())
}

