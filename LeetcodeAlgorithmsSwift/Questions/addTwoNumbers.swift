/*
 2. Add Two Numbers
 https://leetcode.com/problems/add-two-numbers/description/
 
 You are given two non-empty linked lists representing two non-negative integers.
 The digits are stored in reverse order and each of their nodes contain a
 single digit. Add the two numbers and return it as a linked list.
 
 You may assume the two numbers do not contain any leading zero, except the
 number 0 itself.
 
 Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 Output: 7 -> 0 -> 8
 
 Runtime: 106 ms
 */

/*
 class ListNode {
     var val: Int
     var next: ListNode?
 
     init(_ val: Int) {
     self.val = val
     self.next = nil
     }
 }
 */

func addTwoNumbers(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
    let head = ListNode(0)
    var current = head
    var currentL1 = l1, currentL2 = l2
    
    var carry = 0
    while currentL1 != nil || currentL2 != nil {
        let sum = (currentL1?.val ?? 0) + (currentL2?.val ?? 0) + carry
        carry = sum / 10
        
        current.next = ListNode(sum % 10)
        current = current.next!
        
        currentL1 = currentL1?.next
        currentL2 = currentL2?.next
    }
    
    if carry > 0 {
        current.next = ListNode(carry)
    }
    
    return head.next
}
