//
//  twoSumTests.swift
//  Leetcode Algorithms SwiftTests
//


import XCTest
@testable import LeetcodeAlgorithmsSwift

class TwoSumTests: XCTestCase {
    
    func testTwoSum() {
        XCTAssertEqual(twoSum([2, 7, 11, 15], 9), [0, 1])
        XCTAssertEqual(twoSum([3,2,4], 6), [1, 2])
    }
    
}
