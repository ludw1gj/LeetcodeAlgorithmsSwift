//
//  addStringsTests.swift
//  Leetcode Algorithms SwiftTests
//

import XCTest
@testable import LeetcodeAlgorithmsSwift

class AddStringsTests: XCTestCase {
    
    func testAddStrings() {
        XCTAssertEqual(addStrings("15", "28"), "43")
        XCTAssertEqual(addStrings("9", "1"), "10")
        XCTAssertEqual(addStrings("11", "8"), "19")
    }
    
}
