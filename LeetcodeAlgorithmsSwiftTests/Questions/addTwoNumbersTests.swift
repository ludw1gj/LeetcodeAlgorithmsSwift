//
//  addTwoNumbersTests.swift
//  Leetcode Algorithms SwiftTests
//

import XCTest
@testable import LeetcodeAlgorithmsSwift

class addTwoNumbersTests: XCTestCase {
    
    func formatAnswer(head: ListNode?) -> Int {
        var result = [String]()
        
        var currentNode = head
        while currentNode != nil {
            result.append(String(currentNode!.val))
            
            currentNode = currentNode?.next
        }
        
        return Int(result.reversed().joined())!
    }
    
    func testAddTwoNumbers() {
        let l1: ListNode? = {
            let n1 = ListNode(2)
            let n2 = ListNode(4)
            let n3 = ListNode(3)
            
            n1.next = n2
            n2.next = n3

            return n1
        }()
        
        let l2: ListNode? = {
            let n1 = ListNode(5)
            let n2 = ListNode(6)
            let n3 = ListNode(4)
            
            n1.next = n2
            n2.next = n3
            
            return n1
        }()
        
        let result = formatAnswer(head: addTwoNumbers(l1, l2))
        let expect = 807
        
        XCTAssertEqual(result, expect)
    }

}
